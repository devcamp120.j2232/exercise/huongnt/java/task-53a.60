public class Date {
    // khai báo thuộc tính
    public int day;
    public int month;
    public int year;

    // khởi tạo phương thức
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    // getter
    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    // setter
    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    // phương thức khác
    public void setDate(int day, int month, int year) {
        this.year = year;
    }

    // in ra console log
    @Override
    public String toString() {
        return String.format("Date [day = %s, month = %s, year = %s]", day, month, year);
    }

}
