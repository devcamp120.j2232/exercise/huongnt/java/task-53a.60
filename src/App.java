public class App {
    public static void main(String[] args) throws Exception {
                // khởi tạo đối tượng date có tham số
                Date date1 = new Date(12, 01, 2023);
                Date date2 = new Date(30, 12, 2024);

                System.out.println("Date 1");
                System.out.println(date1.toString());
                System.out.println("Date 2");
                System.out.println(date2.toString());
        
    }
}
